const express=require('express');
const app=express();
const path=require('path');
const router=require('./routes/web');
const bodyParser=require('body-parser');
const fs=require('fs');
const sequelize=require('./config/db-config');

async function connect(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } 
    catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}
require('dotenv').config();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('views'));
app.use('/images', express.static('images'));
app.use(router);
connect();
router.get('/',(req,res)=>{
    fs.readFile('./customers/customers.txt','utf8', (err, data) => {
        var arr=data.split('\r\n');
        //console.log(arr);
        var x=[];
        for(var i=0;i<arr.length;i++){
            x.push({customer:arr[i]});
        }
        
        res.render('home',{data:x,duplicates:[]});
    });
});
var port=process.env.PORT || 8000;
app.listen(port,()=>console.log(`server running on port ${port}!`));