const fs=require('fs')
const invoiceModel=require("../models/invoice_model");
const reader = require('xlsx');
const decompress=require('decompress');


// Controller function to manipulate the data before sending it to Database for registering....

exports.checkDuplicates=async(req,res)=>{
    const path="uploads/";                                          // folder where temporarily zip file is stored and extracted
    decompress(path+"input.zip", path).then(files => {              // file is stored as 'input.zip' name...
            //console.log('done!');
           
            }).then(()=>{
                fs.unlink(path+"input.zip", (err) => {
                    if (err) {
                        throw err;
                    }
                    else{
                        addValues(path,req,res);
                    }
                });
            }).catch((err)=>{
                console.log(err);
            });
   
    
}

async function addValues(path,req,res){
    
    const dir = await fs.promises.opendir(path);
    var duplicates=[];
    for await (const dirent of dir) {                             // iterating the directory '/uploads/' for parsing each and every file present...
        
        //console.log(dirent.name);
        if(dirent.name==".gitkeep") continue;
        
        
        var dataDuplicate=await GetDuplicates(req,path+dirent.name,dirent.name)
        duplicates=duplicates.concat(dataDuplicate);
        
        //console.log(dataDuplicate);
        
       
        }
      
        
            res.render('home',{duplicates:duplicates});
        
    }

    // function for inserting the data and storing the data which is already in the database ...
function GetDuplicates(req,fname,client){
    var months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var invoice_month=months[parseInt(req.body.InvoicedMonth.split("-")[1],10)-1]+" "+req.body.InvoicedMonth.split("-")[0];
    
    var results=[];
    var duplicates=[]; 
    
    return new Promise(async (resolve, reject) => {
        
        
        
            //console.log(results[5]);
            results= parse_xlsx_sheets(fname);
            //console.log(results);
            fs.unlink(fname, (err) => {
                if (err) {
                    throw err;
                }
            });
            for(var i=0;i<results.length;i++){
               //console.log(results[i]['S.No.']+"  "+Number.isInteger(parseInt(results[i]['S.No.'])));
                
                if(results[i]['data']['Manuscript'] && Number.isInteger(parseInt(results[i]['data']['S.No.']))){
                    //console.log(results[i]);
                    var customer=results[i]['customer'];
                    //console.log(results[i]['data']['Manuscript']);
                    try{
                    var [record,created]=await invoiceModel.findOrCreate({
                        where: {
                        Customer: customer,
                        ManuscriptId: results[i]['data']['Manuscript']
                        },
                        defaults: {
                            InvoicedMonth: invoice_month
                        }
                    });
                    
                    if(created==false){
                        //console.log(record);
                        duplicates.push({Customer:record.Customer,ManuscriptId:record.ManuscriptId,invoicedMonth:record.InvoicedMonth});
                    }
                }
                catch(err){
                    console.log(err);
                    reject();
                }
                }
            }
            

            resolve(duplicates);
            
        
    });
}

// Function to parse .xlsx files...
function parse_xlsx_sheets(fname){
    
    var file = reader.readFile(fname);

        let data = []
  
        var sheets = file.SheetNames
        //console.log(sheets);
  
        for(let i = 0; i < sheets.length; i++)
        {
            var range = reader.utils.decode_range(file.Sheets[file.SheetNames[i]]['!ref']);
    
            range.s.r = 1; // <-- zero-indexed, so setting to 1 will skip row 0
            file.Sheets[file.SheetNames[i]]['!ref'] = reader.utils.encode_range(range);
            //console.log(range);
            var temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]])
            temp.forEach((res) => {

                data.push({data:res,customer:sheets[i]});
            })
        }
        
    return data;
    
}