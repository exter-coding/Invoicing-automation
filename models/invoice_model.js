// Model of the invoices stored...

const { Sequelize, DataTypes } = require('sequelize');
const sequelize=require('../config/db-config');
const Invoice = sequelize.define('Invoice', {
    
    Customer: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey:true
      
    },
    ManuscriptId: {
      type: DataTypes.STRING,
      allowNull:false,
      primaryKey:true
    },
    InvoicedMonth: {
        type: DataTypes.STRING,
        allowNull:true
    }
  }, {
    freezeTableName: true
  });
module.exports=Invoice;