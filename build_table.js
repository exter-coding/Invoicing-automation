const csvParser=require('csv-parser');
const fs = require('fs')
const results = {};
const sequelize=require('./config/db-config');
const invoiceModel=require('./models/invoice_model');
async function connect(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } 
    catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}
async function insertIntoDB(path) {
    
    const dir = await fs.promises.opendir(path);
    filenames=[];
    for await (const dirent of dir) {
        //console.log(dirent.name)
        filenames.push(dirent.name);
        results[dirent.name]=[];
        fs.createReadStream(path+dirent.name).pipe(csvParser()).on('data', (data) =>results[dirent.name].push(data)).on('end', () => {
            //console.log(results[dirent.name]);
        });

    }
    await invoiceModel.sync({ force: true });
   
   log=[];
    for(var i=0;i<filenames.length;i++){
        var data=results[filenames[i]];
        for(var j=0;j<data.length;j++){
            //console.log(data[j]);
            var Customer=(data[j].hasOwnProperty('Client Name'))?data[j]['Client Name']:data[j]['Customer'];
            var ManuscriptId=(data[j].hasOwnProperty('Article Id'))?data[j]['Article Id']:(data[j].hasOwnProperty('ISBN'))?data[j]['ISBN']:(data[j].hasOwnProperty('Title'))?data[j]['Title']:(data[j].hasOwnProperty('Article number'))?data[j]['Article number']:data[j]['Order number'];
            var InvoicedMonth=(data[j].hasOwnProperty('Invoiced Month'))?data[j]['Invoiced Month']:data[j]['Invoice Month'];
            try{
            await invoiceModel.create({
                Customer,
                ManuscriptId,
                InvoicedMonth
        });
    }
    catch(error){
        log.push({
            err:error.name,
            filename:filenames[i],
            customer:Customer,
            articleid:ManuscriptId
        });
    }
        }
    
    }
    console.log(log);
    fs.writeFile("log.txt", JSON.stringify(log), function(err) {
        if (err) throw err;
        console.log('complete');
        }
    );
  }
connect();
insertIntoDB('./data/').catch(console.error);
//console.log(invoiceModel);