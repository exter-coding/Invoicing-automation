//routes...

const router=require('express').Router();
const {checkDuplicates}=require('../controllers/checkduplicates');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "uploads")
    },
    filename: function (req, file, cb) {
       
        cb(null, `input.zip`)
    }
})
const upload = multer({ storage });
router.post('/submit',upload.single("file"),checkDuplicates);           // for posting the data to the server calls a controller function checkDuplicates()
    
   
module.exports=router;