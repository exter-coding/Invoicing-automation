# Invoicing Automation

This project is to reduce manual work in the invoicing team. The invoicing team spends a considerable number of hours ensuring that articles are not duplicated in the invoice. The project is to automate this process.

## Getting started

To make it easy for you to get started with Project, here's a list of recommended next steps.
### Clone Repository 

```
git clone https://gitlab.com/exter-coding/Invoicing-automation.git
```
### Install the required modules 
```
npm install --save
```
- Set the environmental variables present in `.env` file or use the predefined variables. 
## Name
Duplicate check app 

## Description
- The project is built using the `nodejs` the npm modules used can be viewed from `package.json` file.
    - The project consist of an API and the UI along with the `MySQL` database integration hosted on server provided by `db4free.net`
    - The project can be used for testing purposes.
    - The hosted link for the project is [](https://duplicatecheck02.herokuapp.com/)

## Visuals
* Home Screen 
    ![Home Screen](./input/homeScreen.png?raw=true "home screen view")
* Result 
    ![Result](./input/Result.png?raw=true "Duplicates")



## Usage

### Input format:
- Select the month and year of the invoices from the input field.
- Upload the `.zip` file having all the excel files within it.
    - The excel files should have atleast two fields named `Manuscript` and `S.No.` field present on the second row of the excel sheet as shown below.
    
    ![Excel file format](./input/excel_format.png?raw=true "excel file format")

    - The `Customer` field of the database will take the value same as the name defined for the sheet inside the excel file. Like for the above example value will be "wordCount_ACMI" for the Customer.
- For addtional feature of reconfiguring the database with pre-loaded `.csv` files add those to the `./data/` folder inside the repo and run the following command.
    ```
    node build_table.js
    ```

### Run the server
- After setting the `PORT` variable in `.env` file to some value say 8000.
- To start the server run the following command in the command prompt within the directory where the repo is stored.
    ```
    node index.js
    ```
- Check the server running on http://127.0.0.1:8000

- Project is already hosted on [Heroku](https://duplicatecheck02.herokuapp.com/) for testing purpose.
## Authors 
Promit Revar

## License
MIT

## Project status
The project is ready and can be used for testing purposes but while using the hosted version for the production a new realiable database service has to be employed provided by legitimate DB providers since the project for now uses the free database provided by [db4free](https://www.db4free.net/signup.php).

